(defproject rmq_adapter "0.0.1"
  :main rmq_adapter.core
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [clj-json/clj-json "0.5.3"]
                 [com.novemberain/langohr "5.0.0"]
                 [org.clojure/core.async "0.4.474"]])
