(ns rmq_adapter.core
  (:require [langohr.core       :as rmq]
            [langohr.channel    :as lch]
            [langohr.queue      :as lq]
            [langohr.exchange   :as le]
            [langohr.consumers  :as lcons]
            [langohr.basic      :as lb]
            [clj-json.core      :refer [generate-string parse-string]]
            [clojure.core.async :as a :refer [go <! >! chan]]
            ))

(defn init
  "amqp://guest:guest@127.0.0.1/%2F"
  [queue & [broker_url]]
  (let [conn (if broker_url
               (rmq/connect {:uri broker_url})
               (rmq/connect))
        ch   (lch/open conn)]
    (lq/declare ch queue {:exclusive false :auto-delete true})
    {:connection conn
     :channel ch
     :queue queue}))

(defn to-rmq [queue & [broker_url]]
  (let [ach (chan)
        {:keys [channel]} (init queue broker_url)]
    (go
      (loop []
        (lb/publish channel
                    ""
                    queue
                    (generate-string (<! ach))
                    {:content-type "application/json"})
        (recur)))
    ach))

(defn from-rmq [queue & [broker_url]]
  (let [ach (chan)
        {:keys [channel]} (init queue broker_url)
        handler
        (lcons/ack-unless-exception
         (fn [ch metadata ^bytes payload]
           (let [input-data (parse-string (String. payload "UTF-8") true)]
             (go (>! ach input-data)))))]
    (lcons/subscribe channel queue handler {:auto-ack false})
    ach))



